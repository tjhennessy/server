#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <netdb.h>
#include <mqueue.h>
#include <fcntl.h>
#include "server.h"
#include "receive.h"
#include "send.h"

#define DEBUG 1
#define MQ_CHAT_NAME "/stuff"
#define MAX_CLIENTS 10
#define WELL_KNOWN_PORTS 1024
#define IS_WELL_KNOWN(NUM) ((NUM) <= WELL_KNOWN_PORTS)

typedef struct 
{
    int id;
    int fd;
} client_data;

static client_data clients[MAX_CLIENTS];
static const int MAX_BACKLOG = 4;
static mqd_t msg_q;
static struct mq_attr chat_mq_attr;
static pthread_mutex_t queue_lock;

int get_tcp_server(char *port)
{
    if (IS_WELL_KNOWN(atoi(port)))
    {
        fprintf(stderr, "Error: port number is not in valid range\n");
        return -1;
    }

    struct addrinfo hints;
    struct addrinfo *servinfo;
    struct addrinfo *p;

    memset(&hints, 0, sizeof(hints)); // must be done to avoid errors
    hints.ai_family = AF_UNSPEC;      // IPv4 or IPv6 
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;      // use local IP

    int ret = getaddrinfo(NULL, port, &hints, &servinfo);
    if (ret != 0)
    {
        fprintf(stderr, "getaddrinfo %s\n", gai_strerror(ret));
        return -1;
    }

    // Attempt to get socket for each result in linked list
    // Attempt to bind each successful socket until successful
    int sockfd;
    for (p = servinfo; p  != NULL; p = p->ai_next)
    {
        sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (sockfd < 0)
        {
            continue;
        }

        ret = bind(sockfd, p->ai_addr, p->ai_addrlen);
        if (ret < 0)
        {
            close(sockfd);
            continue;
        }
        
        break; // received a socket and bound to address
    }

    freeaddrinfo(servinfo);

    // There exists a chance where socket was not bound
    //   leaving loop above
    if (NULL == p)
    {
        fprintf(stderr, "Error: server could not bind\n");
        return -1;
    }

    ret = listen(sockfd, MAX_BACKLOG);
    if (ret < 0)
    {
        fprintf(stderr, "Error: call to listen() did not work\n");
        return -1;
    }

    return sockfd;
}

int accept_connections(int server_sockfd)
{
    struct sockaddr_in client_addr;
    socklen_t clnt_addr_len = sizeof(client_addr);
    int client_sockfd = accept(server_sockfd, 
                               (struct sockaddr *) &client_addr, 
                               &clnt_addr_len);
    if (client_sockfd < 0)
    {
        fprintf(stderr, "Error: call to accept() did not work\n");
        return -1;
    }
    
    char clnt_info[INET_ADDRSTRLEN];
    const char *ret = inet_ntop(AF_INET, 
                                &(client_addr.sin_addr.s_addr), 
                                clnt_info, 
                                sizeof(clnt_info));
    if (ret != NULL)
    {
        printf("connection from %s:%d\n", clnt_info, 
                                          ntohs(client_addr.sin_port));
    }

    return client_sockfd;
}

void teardown(int *client_id)
{
    close(*client_id);
    remove_client(*client_id);
    pthread_exit(NULL);
}

void echo(int sockfd)
{
    // send welcome message
    char *welcome_msg = "hello there and welcome to the chat server\n";
    int msg_length = strlen(welcome_msg);
    int ret = sendall(sockfd, welcome_msg, &msg_length);
    if (ret < 0)
    {
        fprintf(stderr, "Error: call to sendall experienced send failure\n");
        return; // cleanup occurs from calling environment
    }

    // This is a test for receive all functionality.
    // The goal is to accurately receive a byte value
    // which corresponds to the number of bytes being sent
    int lengthtest = get_message_length(sockfd);
    printf("Message length is %d\n", lengthtest);
    /*
    char *msg = NULL;
    while (true)
    {
       msg = recvall(sockfd, );
       if (NULL == msg)
       {
           fprintf(stderr, "Error: call to recv() failed\n");
           // how does this change what happens next?
           // continue or fall through to send?
       }

       msg_length = 
    }
    */
}

void *run(void *client_id)
{
    pthread_detach(pthread_self());

    // TODO: convert to a looping recv
    // when recv completes send message to msg_q

    int *cid = (int *) client_id;
    #if DEBUG
        printf("client running on new thread\n");
    #endif

    while (true)
    {
        char buffer[100] = {0};
        ssize_t bytes_recvd = recv(clients[*cid].fd,
                                   buffer, sizeof(buffer) - 1, 0);
        if (bytes_recvd < 0)
        {
            perror("recv call failed");
            break;
        }

        if (0 == bytes_recvd)
        {   // Client closed connection
            break;
        }

        // Use mutex lock
        pthread_mutex_lock(&queue_lock);
        int ret = mq_send(msg_q, buffer, sizeof(buffer) - 1, 1);
        // Signal condition variable message is in queue, do after 
        // testing ret to make sure valid 
        // then unlock mutex 
        if (ret < 0)
        {
            perror("mq_send failure");
            break;
        }
        pthread_mutex_unlock(&queue_lock);

        memset(buffer, 0, sizeof(buffer));
    }

    #if DEBUG
        printf("client exiting\n");
    #endif
    teardown(cid);

    return NULL; // Makes compiler happy
}

void signal_handler(int signum)
{
    #if DEBUG
        printf("caught signal, terminating program\n");
    #endif
    mq_close(msg_q);
    mq_unlink("/stuff");
    pthread_mutex_destroy(&queue_lock);
    exit(0);
}

void *broadcast(void *arg)
{
    pthread_detach(pthread_self());

    #if DEBUG
        printf("broadcasting thread up\n");
    #endif

    while (true)
    {
        char buffer[100] = {0};
        // Should block until condition variable is signalled
        // use mutex lock to protect message queue
        pthread_mutex_lock(&queue_lock);
        ssize_t msg_bytes = mq_receive(msg_q, buffer, sizeof(buffer) - 1, NULL);
        // mutex unlock, reset condition variable
        pthread_mutex_unlock(&queue_lock);
        int i;
        for (i = 0; i < MAX_CLIENTS; i++)
        {
            if (-1 != clients[i].fd)
            {
                ssize_t bytes_sent = send(clients[i].fd, buffer, msg_bytes, 0);
                if (bytes_sent < 0)
                {
                    perror("send failed");
                    continue;
                }
            }
        }
    }
}

void init_clients()
{
    int i;
    for (i = 0; i < MAX_CLIENTS; i++)
    {
        clients[i].id = -1;
        clients[i].fd = -1;
    }
}

int add_client(int sockfd)
{
    int i;
    for (i = 0; i < MAX_CLIENTS; i++)
    {
        if (-1 == clients[i].id)
        {
            clients[i].id = i;
            clients[i].fd = sockfd;
            return i;
        }
    }
    return -1;
}

void remove_client(int uid)
{
    clients[uid].id = -1;
    clients[uid].fd = -1;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        fprintf(stderr, "usage: %s <port number>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    signal(SIGINT, signal_handler);

    init_clients();

    int ret = pthread_mutex_init(&queue_lock, NULL);
    if (ret != 0)
    {
        fprintf(stderr, "Error: could not initialize mutex\n");
        exit(EXIT_FAILURE);
    }

    // make sure this message queue is not running from old instance
    mq_unlink("/stuff");

    // set up message_queue
    chat_mq_attr.mq_maxmsg = 10;
    chat_mq_attr.mq_msgsize = 255;
    msg_q = mq_open("/stuff", O_RDWR | O_CREAT, 0777, &chat_mq_attr);
    if(msg_q == (mqd_t) -1)
    {
        printf("%d\n", msg_q);
        fprintf(stderr, "Error: could not open message queue\n");
        perror("mq_open failure");
        exit(EXIT_FAILURE);
    }

    // create a thread for reading from message queue
    pthread_t broadcast_tid;
    ret = pthread_create(&broadcast_tid, NULL, broadcast, NULL);
    if (ret != 0)
    {
        fprintf(stderr, "Error: could not spin up broadcast thread\n");
        signal_handler(0);
    }

    #if DEBUG
        printf("setting up server now\n");
    #endif

    int server_sockfd = get_tcp_server(argv[1]);
    while (true)
    {
        // TODO: add each client to array of clients
        // Server up, start accepting client connections
        int client_sockfd = accept_connections(server_sockfd);
        if (client_sockfd < 0)
        {
            fprintf(stderr, "Error: could not connect client\n");
            continue;
        }

        int id = add_client(client_sockfd);

        // New client connected, spin off new thread
        pthread_t tid;
        int ret = pthread_create(&tid, NULL, run, &(clients[id].id));
        if (ret != 0)
        {
            fprintf(stderr, "Error: could not spin up new thread\n");
            exit(EXIT_FAILURE);
        }
    }

    close(server_sockfd);

    return 0;
}
