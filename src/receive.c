#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>

// get length of header
int get_message_length(int sockfd)
{
    uint8_t length;
    int num_bytes = recv(sockfd, &length, 1, 0);
    if (num_bytes < 0)
    {
        fprintf(stderr, "Error: could not get length\n");
        return -1;
    }

    return (int) length;
}

char *recvall(int sockfd, int num_bytes_to_recv, int *actual_bytes_recvd)
{
    if (num_bytes_to_recv <= 0)
    {
        *actual_bytes_recvd = 0;
        return NULL;
    }

    char *buffer = (char *) calloc(num_bytes_to_recv + 1, sizeof(char));
    if (NULL == buffer)
    {
        fprintf(stderr, "Error: malloc failed\n");
        exit(EXIT_FAILURE);
    }

    int total_bytes_recvd = 0;
    int bytes_remaining = num_bytes_to_recv;
    while (total_bytes_recvd < num_bytes_to_recv)
    {
        int bytes_recvd = recv(sockfd, 
                               buffer + total_bytes_recvd, 
                               bytes_remaining, 0);
        if (bytes_recvd < 0)
        {
            free(buffer);
            *actual_bytes_recvd = 0;
            return NULL;
        }

        total_bytes_recvd += bytes_recvd;
        bytes_remaining -= bytes_recvd;
    }

    *actual_bytes_recvd = total_bytes_recvd;

    return buffer;
}
