#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>

int sendall(int sockfd, char *buffer, int *bytes_to_send)
{
    int total_bytes_sent = 0;
    int bytes_left_to_send = *bytes_to_send;

    while (total_bytes_sent < *bytes_to_send)
    {
        int bytes_sent = send(sockfd, 
                          buffer + total_bytes_sent,
                          bytes_left_to_send, 0);
        if (-1 == bytes_sent)
        {
            return -1;
        }

        total_bytes_sent += bytes_sent;
        bytes_left_to_send -= bytes_sent;
    }

    *bytes_to_send = total_bytes_sent;

    return 0;
}