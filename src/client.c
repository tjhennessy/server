#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "client.h"
#include "receive.h"
#include "send.h"

int main(void)
{
    // TODO: parse port from command line
    int server_port = 1991;
    int sockfd = socket(PF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        fprintf(stderr, "Error: socket call failed\n");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in server_addr;
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(server_port);
    server_addr.sin_addr.s_addr = inet_addr("127.0.01");

    int ret = connect(sockfd, 
                     (const struct sockaddr *) &server_addr, 
                      sizeof(server_addr));

    if (ret < 0)
    {
        fprintf(stderr, "Error: could not connect to server\n");
        exit(EXIT_FAILURE);
    }

    printf("connection established with server\n");

    // receive welcome msg
    // send length test value
    // just set up shoddy recv function for now
    // goal is to send an byte representing the length of
    // a potential method to follow
    char buffer[256] = {0};
    recv(sockfd, buffer, 256, 0);
    printf("%s\n", buffer);
    uint8_t l = 255;
    int send = 1;
    ret = sendall(sockfd, (char *) &l, &send);
    printf("%hhu sent\n", l);
    close(sockfd);

    return 0;
}