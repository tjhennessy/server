cmake_minimum_required(VERSION 3.5)

include_directories(include/)
find_package(Threads)
add_executable(server 
               src/server.c 
               src/receive.c 
               src/send.c
)

target_link_libraries(server ${CMAKE_THREAD_LIBS_INIT} rt)
target_compile_options(server PUBLIC
    "-std=gnu99"
    "-Wall"
    "-Werror"
    "-lpthread"
)

# start client executable section
add_executable(client 
               src/client.c
               src/receive.c
               src/send.c
)

target_link_libraries(client ${CMAKE_THREAD_LIBS_INIT})
target_compile_options(client PUBLIC
    "-std=gnu99"
    "-Wall"
    "-Werror"
    "-lpthread"
)
