#ifndef SERVER_H
#define SERVER_H

int get_tcp_server(char *port);
int accept_connections(int server_sockfd);
void teardown(int *client_id);
void echo(int sockfd);
void *run(void *client_id);
void signal_handler(int signum);
void *broadcast(void *arg);
void init_clients();
int add_client(int sockfd);
void remove_client(int uid);

/**
 * @brief Entry function for pthread to start chat
 * 
 * @param sockfd is client file descriptor for socket
 *        connection to server
 */
void *run(void *sockfd);

#endif /* SERVER_H */