#ifndef SEND_H
#define SEND_H

/**
 * @brief Wrapper for sockets send call ensuring bytes_to_send
 *        number of bytes is sent from buffer to sockfd
 * 
 * @param sockfd is remote connection 
 * @param buffer is an array of bytes (chars)
 * @param bytes_to_send is the number of bytes in buffer
 * 
 * @returns 0 on success, -1 on error (when send() fails)
 */ 
int sendall(int sockfd, char *buffer, int *bytes_to_send);

#endif /* SEND_H */