#ifndef RECEIVE_H
#define RECEIVE_H
// one point to emphasize in documentation is to free returned
//   char buffer at some point
int get_message_length(int sockfd);
char *recvall(int sockfd, int num_bytes_to_recv, int *actual_bytes_recvd);
#endif /* RECEIVE_H */